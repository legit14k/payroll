﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE07
{
    class Program
    {
        static Random rand = new Random();
        static void Main(string[] args)
        {
            Menu();
            WaitForAnyKey();
        }

        static void Menu()
        {
            Employee newEmployee = null;
            
            bool running = false;
            string userInput = "";

            Console.Title = "Employee Tracking";
            
            PrintCommands();

            while (true)
            {
                PrintCommands();
                Console.Write("\nEnterCommand: ");
                userInput = Console.ReadLine().ToLower();

                switch (userInput)
                {
                    case "1":
                    case "add employee":
                        {
                            running = true;
                            while (running)
                            {
                                Console.Clear();
                                Console.WriteLine("-----------------------");
                                Console.WriteLine("Add Employee Commands: ");
                                Console.WriteLine("-----------------------");
                                Console.WriteLine("1.) Full time");
                                Console.WriteLine("2.) Part time");
                                Console.WriteLine("3.) Contractor");
                                Console.WriteLine("4.) Salaried");
                                Console.WriteLine("5.) Manager");
                                Console.WriteLine("6.) Main Menu");
                                Console.WriteLine("7.) Quit / exit");
                                Console.WriteLine("-----------------------");

                                Console.Write("\nEnter command: ");
                                userInput = Console.ReadLine().ToLower();

                                switch (userInput)
                                {
                                    case "1":
                                    case "full time":
                                        {
                                            
                                            if (newEmployee != null)
                                            {
                                                newEmployee.NewFullTimer();
                                            }
                                            else
                                            {
                                                newEmployee = new Employee("Admin", "");
                                                newEmployee.NewFullTimer();
                                                
                                            }
                                            break;
                                        }

                                    case "2":
                                    case "part time":
                                        {
                                            if (newEmployee != null)
                                            {
                                                newEmployee.NewPartTimer();
                                            }
                                            else
                                            {
                                                newEmployee = new Employee("Admin", "");
                                                newEmployee.NewPartTimer();
                                            }
                                            break;
                                        }

                                    case "3":
                                    case "contractor":
                                        {
                                            if (newEmployee != null)
                                            {
                                                newEmployee.NewContractor();
                                            }
                                            else
                                            {
                                                newEmployee = new Employee("Admin", "");
                                                newEmployee.NewContractor();
                                            }
                                            break;
                                        }

                                    case "4":
                                    case "salaried":
                                        {
                                            if (newEmployee != null)
                                            {
                                                newEmployee.NewSalaried();
                                            }
                                            else
                                            {
                                                newEmployee = new Employee("Admin", "");
                                                newEmployee.NewSalaried();
                                            }
                                            break;
                                        }

                                    case "5":
                                    case "manager":
                                        {
                                            if (newEmployee != null)
                                            {
                                                newEmployee.NewManager();
                                            }
                                            else
                                            {
                                                newEmployee = new Employee("Admin", "");
                                                newEmployee.NewManager();
                                            }
                                            break;
                                        }

                                    case "6":
                                    case "main menu":
                                        {
                                            running = false;
                                            break;
                                        }

                                    case "7":
                                    case "quit":
                                    case "exit":
                                    case "x":
                                        {
                                            return;
                                        }

                                    default:
                                        {
                                            Console.WriteLine("Invalid command: " + userInput);
                                            WaitForAnyKey();
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    case "2":
                    case "remove employee":
                        {
                            if (newEmployee != null)
                            {
                                newEmployee.RemoveEmployee();
                            }
                            else
                            {
                                Console.WriteLine("No employees to display.");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "3":
                    case "display payroll":
                        {
                            if (newEmployee != null)
                            {
                                newEmployee.DisplayFullTimeEmployees();
                                break;
                            }
                            else
                            {
                                Console.WriteLine("No employees to display.");
                                WaitForAnyKey();
                            }
                            break;
                        }

                    case "4":
                    case "quit":
                    case "exit":
                    case "x":
                        {
                            bool trueOrFalse = true;
                            Console.Write("\nAre you sure you want to quit? (y/n): ");
                            string verifyYesOrNo = Console.ReadLine().ToLower();
                            while (trueOrFalse)
                            {
                                switch (verifyYesOrNo)
                                {
                                    case "y":
                                    case "yes":
                                        {
                                            return;
                                        }
                                    case "n":
                                    case "no":
                                        {
                                            trueOrFalse = false;
                                            break;
                                        }
                                    default:
                                        {
                                            trueOrFalse = false;
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    default:
                        {
                            Console.WriteLine("Invalid command: " + userInput);
                            WaitForAnyKey();
                            break;
                        }
                }
            }

        }

        //------------------------------------------------------------------
        static void PrintPayrollValues<t>(List<t> _List)
        {
            if (_List.Count > 0)
            {
                Console.WriteLine("\nEmployees in alphabetical order: \n-----------------------------");
                int i = rand.Next(10000, 99999);
                foreach (t item in _List)
                {
                    Console.WriteLine("Employee Number: " + i + ") Name: " + item + "\n");
                    // Add the payroll with this part
                    ++i;
                }
            }
            else
            {
                Console.WriteLine("Nothing to display! Please add employee.");
            }
            Console.ReadKey();
        }

        //------------------------------------------------------------------
        static void PrintCommands()
        {
            Console.Clear();
            Console.WriteLine("----------------");
            Console.WriteLine("Commands: ");
            Console.WriteLine("----------------");
            Console.WriteLine("1.) Add employee");
            Console.WriteLine("2.) Remove employee");
            Console.WriteLine("3.) Display payroll");
            Console.WriteLine("4.) Quit / exit");
            Console.WriteLine("----------------");
        }

        //------------------------------------------------------------------
        static void WaitForAnyKey()
        {
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey(true);
        }
    }
}
