﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE07
{
    class PartTime : Hourly
    {
        decimal _yearlyPay;

        public decimal YearlyPay { get => _yearlyPay; set => _yearlyPay = value; }

        public PartTime(string nameParam, string addressParam, decimal payPerHour, decimal hoursPerWeek, decimal yearlyPay) : base(nameParam, addressParam, payPerHour, hoursPerWeek)
        {
            YearlyPay = yearlyPay;
        }

        public decimal PartPayPerHour(decimal partPayPerHour)
        {
            PayPerHour = partPayPerHour;
            return PayPerHour;
        }

        public decimal PartTimeHours(decimal partTimeHours)
        {
            HoursPerWeek = partTimeHours;
            return HoursPerWeek;
        }

        //public void NewPartTimer()
        //{
        //    string payHolder;
        //    decimal tempPay;
        //    string hourHolder;
        //    decimal tempHours;

        //    Console.WriteLine("Part time employees must specify how many hours a week.");

        //    Console.Write("Name: ");
        //    string newName = Console.ReadLine();

        //    Console.Write("Address: ");
        //    string newAddress = Console.ReadLine();

        //    Console.Write("Pay per hour: ");
        //    payHolder = Console.ReadLine();

        //    while (!decimal.TryParse(payHolder, out tempPay))
        //    {
        //        Console.Write("Please enter a decimal.\r\nPay per hour: ");
        //        payHolder = Console.ReadLine();

        //        while (string.IsNullOrWhiteSpace(payHolder))
        //        {
        //            Console.Write("Please do not leave blank.\r\nPay per hour: ");
        //            payHolder = Console.ReadLine();
        //        }
        //    }

        //    Console.Write("Hours per week: ");
        //    hourHolder = Console.ReadLine();

        //    while (!decimal.TryParse(hourHolder, out tempHours))
        //    {
        //        Console.Write("Please enter a decimal.\r\nHours per week: ");
        //        hourHolder = Console.ReadLine();

        //        while (string.IsNullOrWhiteSpace(hourHolder))
        //        {
        //            Console.Write("Please do not leave blank.\r\nHours per week: ");
        //            hourHolder = Console.ReadLine();
        //        }
        //    }

        //    Console.WriteLine("You will be working {0} hours per week.", tempHours);

        //    Name = newName;
        //    Address = newAddress;
        //    PayPerHour = tempPay;
        //    HoursPerWeek = tempHours;
        //    Console.WriteLine("Press any key to continue.");
        //    Console.ReadKey();
        //}

        public void DisplayPartTimers()
        {
            Console.WriteLine($"Name: {Name}\nAddress: {Address}\nPay Per Hour: ${PayPerHour}\nHours per week: {HoursPerWeek}\nYearly pay: ${YearlyPay}");
        }
    }
}
