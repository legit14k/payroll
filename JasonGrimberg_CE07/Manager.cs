﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE07
{
    class Manager : Salaried
    {
        private decimal _bonus;

        public Manager(string nameParam, string addressParam, decimal salaryParam, decimal bonusParam) : base(nameParam, addressParam, salaryParam)
        {
            ManagerBonus(bonusParam);
        }

        public decimal Bonus { get => _bonus; set => _bonus = value; }

        public decimal ManagerBonus(decimal managerBonus)
        {
            Bonus = managerBonus;
            return Bonus;
        }

        public void DisplayManagerEmplyees()
        {
            Console.WriteLine($"Name: {Name}\nAddress: {Address}\nHours per week: 40\nBonus: ${Bonus}\nYearly pay: ${Salary}");
        }
    }
}
