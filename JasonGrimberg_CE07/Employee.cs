﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE07
{
    class Employee : IComparable<Employee>
    {
        private string _name;
        private string _address;
        List<FullTime> _fullTime;
        List<PartTime> _partTime;
        List<Contractor> _contractor;
        List<Salaried> _salaried;
        List<Manager> _manager;

        public string Address { get => _address; set => _address = value; }
        public string Name { get => _name; set => _name = value; }
        internal List<FullTime> FullTime { get => _fullTime; set => _fullTime = value; }
        internal List<PartTime> PartTime { get => _partTime; set => _partTime = value; }
        internal List<Contractor> Contractor { get => _contractor; set => _contractor = value; }
        internal List<Salaried> Salaried { get => _salaried; set => _salaried = value; }
        internal List<Manager> Manager { get => _manager; set => _manager = value; }

        public Employee(string nameParam, string addressParam)
        {
            EmployeeName(nameParam);
            EmployeeAddress(addressParam);
            _fullTime = new List<FullTime>();
            _partTime = new List<PartTime>();
            _contractor = new List<Contractor>();
            _salaried = new List<Salaried>();
            _manager = new List<Manager>();
        }
        
        public string EmployeeName(string employeeName)
        {
            Name = employeeName;
            return Name;
        }

        public string EmployeeAddress(string employeeAddress)
        {
            Address = employeeAddress;
            return Address;
        }

        public int CompareTo(Employee other)
        {
            return Name.CompareTo(other.Name);
        }

        public virtual decimal CalculatePay(decimal payPerHour, decimal hoursPerWeek)
        {
            decimal TotalPay;
            TotalPay = (payPerHour * hoursPerWeek) * 52;
            return TotalPay;
        }

        //------------------------------------------------------------------
        public void NewFullTimer()
        {
            string payHolder;
            decimal tempPay;

            Console.WriteLine("Full time employees receive an automatic 40 hours per week.");

            Console.Write("Name: ");
            string newName = Console.ReadLine();

            Console.Write("Address: ");
            string newAddress = Console.ReadLine();

            Console.Write("Pay per hour: ");
            payHolder = Console.ReadLine();

            while (!decimal.TryParse(payHolder, out tempPay))
            {
                Console.Write("Please enter a decimal.\r\nPay per hour: ");
                payHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(payHolder))
                {
                    Console.Write("Please do not leave blank.\r\nPay per hour: ");
                    payHolder = Console.ReadLine();
                }
            }

            decimal totalPay = CalculatePay(tempPay, 40);
            
            FullTime fullTimer = new FullTime(newName, newAddress, tempPay, 40, totalPay);
            FullTime.Add(fullTimer);
            FullTime.Sort();
            Console.WriteLine($"Added: {newName} to full time employee list with ${tempPay} per hour.");
            Console.WriteLine("\nPay per year: $" + totalPay.ToString());
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        //------------------------------------------------------------------
        public void NewPartTimer()
        {
            string payHolder;
            decimal tempPay;
            string hourHolder;
            decimal tempHours;

            Console.WriteLine("Part time employees must specify how many hours a week.");

            Console.Write("Name: ");
            string newName = Console.ReadLine();

            Console.Write("Address: ");
            string newAddress = Console.ReadLine();

            Console.Write("Pay per hour: ");
            payHolder = Console.ReadLine();

            while (!decimal.TryParse(payHolder, out tempPay))
            {
                Console.Write("Please enter a decimal.\r\nPay per hour: ");
                payHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(payHolder))
                {
                    Console.Write("Please do not leave blank.\r\nPay per hour: ");
                    payHolder = Console.ReadLine();
                }
            }

            Console.Write("Hours per week: ");
            hourHolder = Console.ReadLine();

            while (!decimal.TryParse(hourHolder, out tempHours))
            {
                Console.Write("Please enter a decimal.\r\nHours per week: ");
                hourHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(hourHolder))
                {
                    Console.Write("Please do not leave blank.\r\nHours per week: ");
                    hourHolder = Console.ReadLine();
                }
            }

            decimal totalPay = CalculatePay(tempPay, tempHours);
            PartTime partTimer = new PartTime(newName, newAddress, tempPay, tempHours, totalPay);
            PartTime.Add(partTimer);
            PartTime.Sort();
            Console.WriteLine("You will be working {0} hours per week.\n", tempHours);
            Console.WriteLine($"Added: {newName} to part time employee list with ${tempPay} per hour.\n");
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        //------------------------------------------------------------------
        public void NewContractor()
        {
            string noBonusHolder;
            decimal tempNoBonus;
            string payHolder;
            decimal tempPay;
            string hourHolder;
            decimal tempHours;

            Console.Write("Name: ");
            string newName = Console.ReadLine();

            Console.Write("Address: ");
            string newAddress = Console.ReadLine();

            Console.Write("Pay per hour: ");
            payHolder = Console.ReadLine();

            while (!decimal.TryParse(payHolder, out tempPay))
            {
                Console.Write("Please enter a decimal.\r\nPay per hour: ");
                payHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(payHolder))
                {
                    Console.Write("Please do not leave blank.\r\nPay per hour: ");
                    payHolder = Console.ReadLine();
                }
            }

            Console.Write("No benefits bonus: ");
            noBonusHolder = Console.ReadLine();

            while(!decimal.TryParse(noBonusHolder, out tempNoBonus))
            {
                Console.WriteLine("Please enter a decimal.\r\nNo benefits bonus: ");
                noBonusHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(payHolder))
                {
                    Console.Write("Please do not leave blank.\r\nNo benefits bonus: ");
                    noBonusHolder = Console.ReadLine();
                }
            }

            Console.Write("Hours per week: ");
            hourHolder = Console.ReadLine();

            while (!decimal.TryParse(hourHolder, out tempHours))
            {
                Console.Write("Please enter a decimal.\r\nHours per week: ");
                hourHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(hourHolder))
                {
                    Console.Write("Please do not leave blank.\r\nHours per week: ");
                    hourHolder = Console.ReadLine();
                }
            }
            decimal totalPay = CalculatePay(tempPay, tempHours);

            Contractor contractor = new Contractor(newName, newAddress, tempPay, tempHours, tempNoBonus, totalPay);
            Contractor.Add(contractor);
            FullTime.Sort();
            Console.WriteLine($"Added: {newName} to contract employee list with ${tempPay} per hour.");
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        //------------------------------------------------------------------
        public void NewSalaried()
        {
            string salaryHolder;
            decimal tempSalary;

            Console.WriteLine("Salaried employees receive an automatic 40 hours per week.");

            Console.Write("Name: ");
            string newName = Console.ReadLine();

            Console.Write("Address: ");
            string newAddress = Console.ReadLine();

            Console.Write("Salary: ");
            salaryHolder = Console.ReadLine();

            while (!decimal.TryParse(salaryHolder, out tempSalary))
            {
                Console.Write("Please enter a decimal.\r\nSalary: ");
                salaryHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(salaryHolder))
                {
                    Console.Write("Please do not leave blank.\r\nSalary: ");
                    salaryHolder = Console.ReadLine();
                }
            }

            Salaried salaried = new Salaried(newName, newAddress, tempSalary);
            Salaried.Add(salaried);
            Salaried.Sort();
            Console.WriteLine($"Added: {newName} to salaried employee list with ${tempSalary} per year.");
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        //------------------------------------------------------------------
        public void NewManager()
        {
            string salaryHolder;
            decimal tempSalary;
            string bonusHolder;
            decimal tempBonus;

            Console.WriteLine("Manager employees receive an automatic 40 hours per week.");

            Console.Write("Name: ");
            string newName = Console.ReadLine();

            Console.Write("Address: ");
            string newAddress = Console.ReadLine();

            Console.Write("Salary: ");
            salaryHolder = Console.ReadLine();

            while (!decimal.TryParse(salaryHolder, out tempSalary))
            {
                Console.Write("Please enter a decimal.\r\nSalary: ");
                salaryHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(salaryHolder))
                {
                    Console.Write("Please do not leave blank.\r\nSalary: ");
                    salaryHolder = Console.ReadLine();
                }
            }

            Console.Write("Bonus: ");
            bonusHolder = Console.ReadLine();

            while(!decimal.TryParse(bonusHolder, out tempBonus))
            {
                Console.WriteLine("Please enter a decimal.\r\nBonus: ");
                bonusHolder = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(bonusHolder))
                {
                    Console.Write("Please do not leave blank.\r\nBonus: ");
                    salaryHolder = Console.ReadLine();
                }
            }

            Manager manager = new Manager(newName, newAddress, tempSalary, tempBonus);
            Manager.Add(manager);
            Manager.Sort();
            Console.WriteLine($"Added: {newName} to manager employee list with ${tempSalary} per year.");
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        //------------------------------------------------------------------
        public void DisplayFullTimeEmployees()
        {
            Console.Clear();
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("Full time employees");
            Console.WriteLine("-----------------------------------------");
            if (FullTime != null)
            {
                foreach (FullTime f in _fullTime)
                {
                    f.DisplayFullTimers();
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("No full time employees.");
            }

            DisplayPartTimeEmployees();
            DisplayContractors();
            DisplaySalaried();
            DisplayManagers();

            Console.WriteLine("-----------------------------------------");
            WaitForAnyKey();
        }

        //------------------------------------------------------------------
        public void DisplayPartTimeEmployees()
        {
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("Part time employees");
            Console.WriteLine("-----------------------------------------");

            if (PartTime != null)
            {
                foreach (PartTime p in _partTime)
                {
                    p.DisplayPartTimers();
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("No part time employees.");
            }
            
        }

        //------------------------------------------------------------------
        public void DisplayContractors()
        {
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("Contract employees");
            Console.WriteLine("-----------------------------------------");

            if(Contractor != null)
            {
                foreach (Contractor c in _contractor)
                {
                    c.DisplayContractEmployees();
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("No contract employees.");
            }
            
        }

        //------------------------------------------------------------------
        public void DisplaySalaried()
        {
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("Salaried employees");
            Console.WriteLine("-----------------------------------------");

            if (Salaried != null)
            {
                foreach (Salaried s in _salaried)
                {
                    s.DisplaySalariedEmployees();
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("No salaried employees.");
            }
        }

        //------------------------------------------------------------------
        public void DisplayManagers()
        {
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("Managers");
            Console.WriteLine("-----------------------------------------");

            if (Manager != null)
            {
                foreach (Manager m in _manager)
                {
                    m.DisplayManagerEmplyees();
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("No managers.");
            }
        }

        //------------------------------------------------------------------
        public void RemoveEmployee()
        {
            int index = SelectList("Select employee to remove (Enter the number): ");

            if (index == -1)
            {
                WaitForAnyKey();
            }
            else
            {
                //Console.WriteLine($"{index + 1}. {FullTime[index]}");
                FullTime.RemoveAt(index);
                Console.WriteLine("Removed selection.");
                WaitForAnyKey();
            }
        }

        //------------------------------------------------------------------
        public int SelectList(string message)
        {
            int len = FullTime.Count;
            int index = -1;

            if (len > 0)
            {
                for (index = 0; index < len; ++index)
                {
                    Console.WriteLine($"{index + 1}. {FullTime.ToList()[index]}");
                }
                Console.Write("\n" + message);
                string selection = Console.ReadLine();
                while (!int.TryParse(selection, out index) || (index < 1 || index > len))
                {
                    Console.Write("Please make a valid selection: ");
                    selection = Console.ReadLine();
                }

                --index;
            }

            return index;
        }

        //------------------------------------------------------------------
        static void PrintPayrollValues<t>(List<t> _List)
        {
            if (_List.Count > 0)
            {
                Console.WriteLine("\nEmployees in alphabetical order: \n-----------------------------");
                int i = 1;
                foreach (t item in _List)
                {
                    Console.WriteLine("Employee Number: " + i + ") Name: " + item + "\n");
                    // Add the payroll with this part
                    ++i;
                }
            }
            else
            {
                Console.WriteLine("Nothing to display! Please add employee.");
            }
            Console.ReadKey();
        }

        //------------------------------------------------------------------
        static void WaitForAnyKey()
        {
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey(true);
        }
    }
}
