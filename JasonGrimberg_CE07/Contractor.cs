﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE07
{
    class Contractor : Hourly
    {
        private decimal _noBenefitsBonus;
        decimal _yearlyTotal;

        public Contractor(string nameParam, string addressParam, decimal payPerHour, decimal hoursPerWeek, decimal noBenefitsBonusParam, decimal yearlyTotal) : base(nameParam, addressParam, payPerHour, hoursPerWeek)
        {
            YearlyTotal = yearlyTotal;
            NoBenefits(noBenefitsBonusParam);
        }

        public decimal NoBenefitsBonus { get => _noBenefitsBonus; set => _noBenefitsBonus = value; }
        public decimal YearlyTotal { get => _yearlyTotal; set => _yearlyTotal = value; }

        public decimal NoBenefits(decimal noBenefits)
        {
            NoBenefitsBonus = noBenefits;
            return NoBenefitsBonus;
        }

        public void DisplayContractEmployees()
        {
            Console.WriteLine($"Name: {Name}\nAddress: {Address}\nPay Per Hour: ${PayPerHour}\nBonus: {NoBenefitsBonus}\nHours per week: {HoursPerWeek}\nYearly pay: ${YearlyTotal}");
        }
    }
}
