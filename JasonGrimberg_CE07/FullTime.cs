﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE07
{
    class FullTime : Hourly
    {
        decimal _yearlyPay;

        public decimal YearlyPay { get => _yearlyPay; set => _yearlyPay = value; }

        public FullTime(string nameParam, string addressParam, decimal payPerHour, decimal hoursPerWeek, decimal yearlyPay) : base(nameParam, addressParam, payPerHour, hoursPerWeek)
        {
            YearlyPay = yearlyPay;
        }

        public decimal FullPayPerHour(decimal fullPayPerHour)
        {
            PayPerHour = fullPayPerHour;
            return PayPerHour;
        }

        public decimal FullTimeHours()
        {
            HoursPerWeek = 40.0m;
            return HoursPerWeek;
        }

        //public void NewFullTimer()
        //{
        //    string payHolder;
        //    decimal tempPay;

        //    FullTimeHours();
        //    Console.WriteLine("Full time employees receive an automatic {0} hours per week.", HoursPerWeek);

        //    Console.Write("Name: ");
        //    string newName = Console.ReadLine();

        //    Console.Write("Address: ");
        //    string newAddress = Console.ReadLine();

        //    Console.Write("Pay per hour: ");
        //    payHolder = Console.ReadLine();

        //    while (!decimal.TryParse(payHolder, out tempPay))
        //    {
        //        Console.Write("Please enter a decimal.\r\nPay per hour: ");
        //        payHolder = Console.ReadLine();

        //        while (string.IsNullOrWhiteSpace(payHolder))
        //        {
        //            Console.Write("Please do not leave blank.\r\nPay per hour: ");
        //            payHolder = Console.ReadLine();
        //        }
        //    }
        //    Name = newName;
        //    Address = newAddress;
        //    PayPerHour = tempPay;
        //    Console.WriteLine("Press any key to continue.");
        //    Console.ReadKey();
        //}

        public void DisplayFullTimers()
        {
            Console.WriteLine($"Name: {Name}\nAddress: {Address}\nPay Per Hour: {PayPerHour}\nHours per week: 40\nYearly pay: {YearlyPay}");
        }
    }
}
