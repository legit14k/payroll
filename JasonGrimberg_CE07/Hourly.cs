﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE07
{
    class Hourly : Employee
    {
        private decimal _payPerHour;
        private decimal _hoursPerWeek;

        public Hourly(string nameParam, string addressParam, decimal payPerHour, decimal hoursPerWeek) : base(nameParam, addressParam)
        {
            PayPerHour1(payPerHour);
            HoursPerWeek1(hoursPerWeek);

        }

        public decimal PayPerHour { get => _payPerHour; set => _payPerHour = value; }
        public decimal HoursPerWeek { get => _hoursPerWeek; set => _hoursPerWeek = value; }

        public decimal PayPerHour1(decimal payPerHour)
        {
            PayPerHour = payPerHour;
            return PayPerHour;
        }

        public decimal HoursPerWeek1(decimal hoursPerWeek)
        {
            HoursPerWeek = hoursPerWeek;
            return HoursPerWeek;
        }

        
    }
}
