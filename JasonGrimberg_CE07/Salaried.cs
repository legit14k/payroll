﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE07
{
    class Salaried : Employee
    {
        private decimal _salary;

        public Salaried(string nameParam, string addressParam, decimal salaryParam) : base(nameParam, addressParam)
        {
            NewSalary(salaryParam);
        }

        public decimal Salary { get => _salary; set => _salary = value; }

        public decimal NewSalary(decimal newSalary)
        {
            Salary = newSalary;
            return Salary;
        }

        public void DisplaySalariedEmployees()
        {
            Console.WriteLine($"Name: {Name}\nAddress: {Address}\nHours per week: 40\nYearly pay: ${Salary}");
        }
    }
}
